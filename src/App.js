import React from 'react';
import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Header from "../src/Components/Header"
import PopularPosts from "../src/Components/PopularPosts"
import RecentPosts from "../src/Components/RecentPosts"
import FrequentPosts from "../src/Components/FrequentPosts"
import Footer from "../src/Components/Footer"
import './App.css';

const App = () => {
  return (
    <div style={{fontFamily: "Arial, Helvetica, sans-serif"}}>
      <Router>
          <Route path="/" >
              <Header />
              <PopularPosts />
              <RecentPosts />
              <FrequentPosts />
              <Footer />
          </Route>
      </Router>
    </div>
  );
}

export default App;
