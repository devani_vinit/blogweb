import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "../css/style.css"
import "../css/common.css"

class Header extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {/*<nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a className="navbar-brand" href="#">Blog</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Features</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Pricing</a>
                            </li>
                        </ul>
                        <span className="navbar-text">Navbar text with an inline element</span>
                    </div>
                </nav>*/}
                <header className="site-navbar" role="banner">
                    <div className="container-fluid">
                        <div className="row align-items-center">
                            <div className="col-12 search-form-wrap js-search-form">
                                <form method="get" action="#">
                                    <input type="text" id="s" className="form-control" placeholder="Search..."/>
                                        <button className="search-btn" type="submit"><span
                                            className="icon-search"/></button>
                                </form>
                            </div>
                            <div className="col-4 site-logo">
                                <a href="index-2.html" className="text-black h2 mb-0">Blog</a>
                            </div>
                            <div className="col-8 text-right">
                                <nav className="site-navigation" role="navigation">
                                    <ul className="site-menu js-clone-nav mr-auto d-none d-lg-block mb-0">
                                        <li><a href="category.html">Home</a></li>
                                        <li><a href="category.html">Politics</a></li>
                                        <li><a href="category.html">Tech</a></li>
                                        <li><a href="category.html">Entertainment</a></li>
                                        <li><a href="category.html">Travel</a></li>
                                        <li><a href="category.html">Sports</a></li>
                                        <li>
                                            <label className="expandSearch">
                                                <input type="text" placeholder="Search..." name="search"/>
                                                <i className="fa fa-search"/>
                                            </label>
                                        </li>
                                        {/* <li className="d-lg-inline-block"><a href="#"
                                                                                    className="js-search-toggle"><img
                                            className="icon-search" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0-Jac5ZM6zLI-D-dvWKT9yQ7fGIX2RbnDWoDErg2O86EMpdHE&s" style={{width: "15px", height: "15px"}}/></a>
                                            <label className="expandSearch">
                                                <input type="text" placeholder="Search..." name="search"/>
                                                    <i className="fa fa-search"/>
                                            </label>
                                        </li> */}
                                    </ul>
                                </nav>
                                <a href="#"
                                   className="site-menu-toggle js-menu-toggle text-black d-inline-block d-lg-none"><span
                                    className="icon-menu h3"/></a></div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default Header;