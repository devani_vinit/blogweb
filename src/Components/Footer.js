import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "../css/style.css"

class Footer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="site-footer">
                    <div className="container">
                        <div className="row mb-5">
                            <div className="col-md-4">
                                <h3 className="footer-heading mb-4">About Us</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat reprehenderit
                                    magnam deleniti quasi saepe, consequatur atque sequi delectus dolore veritatis
                                    obcaecati quae, repellat eveniet omnis, voluptatem in. Soluta, eligendi,
                                    architecto.</p>
                            </div>
                            <div className="col-md-3 ml-auto">
                                {/* <ul className="list-unstyled float-left mr-5">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Advertise</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">Subscribes</a></li>
                                </ul>
                                <ul className="list-unstyled float-left">
                                    <li><a href="#">Travel</a></li>
                                    <li><a href="#">Lifestyle</a></li>
                                    <li><a href="#">Sports</a></li>
                                    <li><a href="#">Nature</a></li>
                                </ul> */}
                            </div>
                            <div className="col-md-4">
                                <div>
                                    <h3 className="footer-heading mb-4">Connect With Us</h3>
                                    <p>
                                        <span href="#" style={{padding: "10px"}}><span className="fa fa-facebook"/></span>
                                        <span href="#" style={{padding: "10px"}}><span className="fa fa-twitter"/></span>
                                        <span href="#" style={{padding: "10px"}}><span className="fa fa-instagram"/></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 text-center">
                                <p>

                                    Copyright &copy;
                                    <script type="6135d8b8aa9a28e6c3e92385-text/javascript">document.write(new
                                        Date().getFullYear());
                                    </script>
                                    All rights reserved | This template is made with <i
                                    className="icon-heart text-danger" aria-hidden="true"/> by <a
                                    href="http://bvminfotech.com/" target="_blank">BVM Infotech</a>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;