import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {fetchRecentPosts} from "../helpers/apiHelper"
import ReactPaginate from 'react-paginate'
import "../css/style.css"
import "../css/common.css"

// const allPosts = require("../helpers/data")
const pageLength = 6
class RecentPosts extends Component {

    constructor(props) {
        super(props)
         this.state = {
            allPosts: []
        }
    }

    async componentWillMount() {
        const allPosts = await fetchRecentPosts({pageSize: pageLength, page: 0})
        this.setResponse(allPosts)
    }

    handlePageClick = async (page) => {
        const allPosts = await fetchRecentPosts({pageSize: pageLength, page: page.selected})
        this.setResponse(allPosts)
    }

    setResponse = (allPosts) => {
        const totalPost = allPosts && allPosts.data
        let totalPage = 0
        if (allPosts && Object.keys(allPosts)) {
            totalPage = Math.ceil(allPosts.metadata[0].total / pageLength)
        }
        this.setState({
            allPosts: totalPost,
            totalPage
        })
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row mb-5">
                        <div className="col-12">
                            <h2>Recent Posts</h2>
                        </div>
                    </div>
                    <div className="row">
                        {
                            (this.state.allPosts && this.state.allPosts.length) ? this.state.allPosts.map(f =>
                                <div className="col-lg-4 mb-4">
                                    <div className="entry2">
                                        <a href={f.url} target="_new"><img src={f.image} alt="Image" className="img-fluid rounded recent-post-img"/></a>
                                        <div className="excerpt example">
                                            {/*<span className="post-category text-white bg-secondary mb-3">Politics Sports</span>*/}
                                            <span className="post-category text-white bg-danger mb-3">Sports</span>&nbsp;&nbsp;
                                            <span className="post-category text-white bg-secondary mb-3">Tech</span>
                                            <h2 /*className="title-wrap"*/ style={{fontSize: "18px", lineHeight: 1.2}}><a href={f.url} target="_new">{f.title}</a></h2>
                                            <div className="post-meta align-items-center text-left clearfix">
                                                {/*<figure className="author-figure mb-0 mr-3 float-left recent-post-figure-img"><img src="https://images.pexels.com/photos/532168/pexels-photo-532168.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="Image" className="img-fluid"/></figure>*/}
                                                <span className="d-inline-block mt-1">By {f.publisher}</span>
                                                <span>&nbsp;-&nbsp; {f.updatedAt && f.updatedAt.split("T")[0]}</span>
                                            </div>
                                            <p className="content-wrap"style={{fontSize: "14px"}}>{f.description}</p>
                                            {/*<p><a href={f.url} target="_new">Read More</a></p>*/}
                                        </div>
                                    </div>
                                </div>
                            ) : null
                        }
                    </div>
                </div>
                <ReactPaginate
                    previousLabel={'prev'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={this.state.totalPage || 0}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />
            </div>
            /*<div>
                <div className="container">
                    <div className="row mb-5">
                        <div className="col-12">
                            <h2>Recent Posts</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <img src="https://images.pexels.com/photos/74512/pexels-photo-74512.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Image" className="img-fluid rounded recent-post-img"/>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-secondary mb-3">Politics</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left recent-post-figure-img"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFRUXFxUXFhUVFRUVFRYWFRUXFhYYFxUYHSggGBolHRUWITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGi0mHyUuLS0tKy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAQMEBQYABwj/xAA+EAABAwIDBQYEAwcFAAMBAAABAAIRAyEEMUEFElFhcQYigZGhsRMywfBCUtEHI2JykuHxFDOCorJDwtIW/8QAGgEAAgMBAQAAAAAAAAAAAAAAAQQAAgMFBv/EACsRAAICAQQBAgUFAQEAAAAAAAABAhEDBBIhMUEiURMyQmFxBRWBwfBSM//aAAwDAQACEQMRAD8A8kSrgiAVzMUIwhCMIlRQnGoGhONCgBxqfphNsCephXQCdhwniEGHCehaxMWLSan6jbK22F2bqV4ce4ziRd38o+uS2GE2FRpWDAXDVw33+WTetlTJqoY+O2WhgnPk82Gya1T5KZPOLJ5nZStm4tHiPot9tbarKBhz2g/l+d8fytIDfMrJ7T7SAzDHHm87v/UadUlLUSk+ENrCl2UeI2G5knelVznbhuCpuI2gCZNMjm18eXdj1TDm712u3h+KRuub1H3kis0vJHiiS8DUa7I34aqyDIWWoVmG0Ges/TirTC4xzIvI5/qVvDUrqSMZ6d/SW+6hcEVGoHCQueE0mnyhRprhjRSI4QlEIBQkIyEJCARspIRkJFAjZCApwhAVRhQK5LC5VCY5KFwCVYjDFCIIQjCIGEE4wIAnWKIA8xPUwmmJ+mroBPoCy2fY/sx8WK1Udz8LT+Ln0VT2P2KcRUG8P3bbvPHg3x/VeuUqQa0NAgCBA0HBZZ821bUWxY9ztkdtMAW7rRrqeTRp1UDaAlsA7jP4TDj1IurSs3e6cPbz9emdfjQBmA5w/CflbwLuJ9B5Tz2hxGOx+CDQSymGjMvfAHiTc+pWZx3wmmHOJ/lEDzi6te1W24NocfzOsOjRw8liK+Ne46f0/orRC0WLxRNg4jx+mqq8Q19N3dgnQjJw5hN/6V7r/fkp1Og9o72lx5QtCbStwjXipvCYysbgaSFoHNDhMjmRYjkRw++aYwGGBdMweOvQ8fMfRP1wWuNrjPgR9/eaLZWiVgDu209uCsCFX4YQN4XAzH8Ofpf3Vm1siRp7LXBl2yp9C+fHuVrsjkISjKEhdEQAISEIkhCAQCEMI0hULAJtwThCFwVWFDS5FCVVCY1cuShYDDFCIJEoRAONTjE21OsUAPsUzB0C9waNSobFsf2e4AVK2+64YJ8dPZGUtqsiVuj0nspskUaTWxkN5382gVy4eM26z+vsupN3Wgam5SbwBLjk0epzPlA8SufJ2xuKpCYh27DW3cZv7n29Fie2G2RRb8Jg3nm5Gt8i76DgtFtnaHwaTqpA33WaDxMwDyAknoeKyuyNguqO+LUkkmZOZOpPP70Wbd8DOLFfLMdS2RVrO3n6+qs6PZ2Mmr0mjsloFggqYQAxCPqNVGJgBsMgZKFjdmnJehV8OFT4vCgobmizgqMHTolhh2Wh4RrPC2fKesmrR32jRw8bjMdI9PBWONw8SCOY8M/vkq2nXh2fCCdDoTyvB5dFdSsWnGh7C0YBMfevl/dOYf8AdugnunLp926Qn2tvby6W89DxslxFG1tBI1tr6eylmLQ1iaW66PEdEyVNxAlgPD2OnmoS6uGe6CZzcsdsgSkRIVqZgkJESQqBQBCEhGQhKBZAwuSrlUhiUoSJQlxlhImoUTUQDgRtQNRtRQB9q9T/AGVYOaVR2peB4RP1XljF7L+zAbuG5lwJ8iPYBZ5X6S0OzZvNyfBRq+jR1J6XJ8ypDjrwv+iqdpYosZa732aBmQP7keaQkx2EbZWYumcVid3/AOOlbkXG59h4NHFaajh2tbAUfYmzxSpgfiN3HmVPcVeEaVsZk/pXSGoUSsNVLJTFUKzRVFZiAq3FNVpiGqvexZNGxQ7Qw8yRmsptKhFxbXodfDktrjis9tbDyCRwQXDM5q0Rtl194BuuXTgOYz8JVuGSydWnLoYI++KxezsSadTd5+I1jmOHlyW/wpDwHDJ4v/NkfT2V2hSRV1GR3dO8P/yq5XWMZAB6e9lTOT+kfDEdSuUIUJRFCU2LCISiSFQIKEoikKDCgEqVcgEw6IIQiCWGWcjahRBEA4EYTYRhQBJoiTC9v7B4M08OyRmA53qB6Ly/sbsB2KqZdwRvHSOBOi9uaG0mDRoEAcT0S2oyJcG+GDfIOPrR3Zubk/lGp8BJVJgaxq1XVQ0lrTutHBrcvqY4+CHbmPj92PnfG9/CCRDfWSoO29s1MJQDaLJde9oGpJ/VJRe6XJ0YR2xs0jtuBtnU3jnumE7Q2myp8p8DZYPbODxbcMK78UTULHVNykD3oiGMv33d4XsMrXUrsti60tbVIfJIDsnWJB65HnnnCYlaKQlCXCRuw60qtx+0GtEk2VhiBDJWF7Q4s70CTH3CEmXivJLrbec61NhPM5ea6K7xJLG9D/ZVOz9lVMQ17y7dDWkgbpdMCQGNyOneIdNwAstiMJUY+oWYktDXOAdLQ17WtaWvhkbrXOJaBM2lFY21ZSWdRlVGtxzKrbuAcOLc/JRqbpCa7ObSq1WhtWZ4kZqfjMLuXCzZonfJiu1eGax7XiwNjy4GVpOx+IlhY65EOHh/b2hU/bBs0lG7D44hwBuWHdni03b+n+Vb6bMMi9VG12rS7hI0M/fmsy5bXHUhumMiFjMWwtcmNNk2yoRzwtWNpClSFdIRESJUihYRA5GUDkGRCSuSLlUsYoIkgSpcYFCIIQiCJUJOUGguAJgEiTwE3KbR0zBB5qEPfez9GlSosbRaXNAEGN1pMfMSdSm8dtQl+6wh9TSP9unz5nPoqTYm13Yxg3GuawWM6EaFxPsrKo1tNsNzIucp5N4DiVxp7raZ04VXBH2bhQ/ENaTvEAvcTcuiwPm4noQtTWwLYiJWZ7NAtxJJzcx3gZYYHCzRbqtpTErXFFNG7fBmq2wGEbrGBrcw3eIYCRE7kROfmn9k7CFMyYtkIy81b1iAUdJwjNbV7leuhvHOhkLCV6W9UIOpWz2pUhkrG1cS0uuYQkWguCzw2ywG2A8gof8A/OtDp3QTxMu98le7JqggCQQrCu0AKJcEa5KGhs9lMXH0VbtqpIgK12lUsVl8ZUmyo0aKJQdoqk0XSs/2ef8AvAQYB7ruQJs7wdBVp2hcd2OfsqnZnccHQIN45Gzgev6q8flYtl5kex4HvUw11jE9CDDh4GVmO0GDLHb2mqv9l1N6kx4PASb3I3QT17vmeKTaMPZcWNj/AAngeXNVg6FpoxTHSEpR4ijuOLdE2uxinuimc2cdrOXLkiuUBQuRlCQgWQMLkULlUhiAlCRKlxgJqJC1EEQBBLCEJxQhtP2d4p2/8GxDjN5tA0GS9JZhA4TmQ4ieI0Xnn7NqHefUP4RDepzXpmzWHdjiZ8dPZc/UJPIOYW9hAwdDdqNf/EfUbvuVoBXAGaq9s/u6bi3Ngkf8L+4Q4xxLd5hkESOhEhZ4/TaHYvcRtr7VDT9BmeSsdnGGbz8zHhyWW2ZQ+JU+JVMQ4hrTpGp5rV0K1Mjd328I3gtFI0l1SGts4imGGXZLDuxNOXEZnIa+S2W0NmMdpIWfr7OY10CB4hRtEgn4I+xsU9l4tOXALSf68OFjKz9VzGageIUeniwT3XXytB80LLWiw2jWlUFdyusVhzuhx1APmJVVVZAlC7JKRh+2eKLPhtaYJJPgIH1UHZrj3d7N0nwBA9SUXa07+KazQME8t5xJ9IR4dv7xnOnI5d//AAtq9KEJO5M9S7KVJpmm7I/WwPnCkYx27vTkbPHPj7qv7NuG43mPQqy2oCWlw+cfMNHR+ogz0WCBNcmX2lTk2015KGpGIqA5ZHzGWajro6N+liGpXKOSLlycFxCkSlIoQ5cuXKobMOlSBKlhgIIwmwnAiAII2hAFZbA2f/qK7KcwCbngBmo3SsiN9+zzBPNLuiAXGSL+sr0rBYYMbPD7P3zVXsj4dJjaVJsNbbqVa16oIj8IzPsBzK57knJtDkU0kmUPaSvDI1ed1o/hF3HxsFV9itsNrsfQce/RMczTPynwMjwCDb2M33PrGzWAtZ1g5e/gvK9m7WqYes3EU/mBMtOTm/iYeWXQgHRUirGFKqPbMTs9rniQCCCCNPHqJVLgNjsweJILScLUG6WtaD8J8jcfugSWkWJGVjlJF12f2tTxdJtWmbHMH5muGbXDiFPx2HLocLuGhyI4Ix4GYyT4b78kbGdnKL2udTrOAi264EZXkmTE81WbR7M4SkA6pVduhrid54G8QLQRHMqbi2YdwO8H03HPdLm+rbLPbSwlAmwqViNXl7x/3MeK1tLwXjjyS+t1+P7KTH0cOSG4dvxDusBffc3pJed7U5NgZSVYdl9jMoS7NxzcYkp6nhzmWho4DNWmzGD5jk1ZzlwBpLi7+47tggQ3WAOgWZ2tXDRGUCTyCn4rGy59Q9B0CxXajGE03nQg+XBVirZnOVIxxxfxsRUqfmdbp8o9IWhbRhmHq6XpOPDPPzWRwzt17T5+Oa9O7OYZuIw9RggmzwNCRNx1g/TKzOTgQiy/2IP3dswJH3wz81bYklzA5uYz8Ps+ZVHsaqKRaCe64d0nQ6g+MevRXVR27JHy6jglOmatWjKY1omRbiOfJRVbbZog95viqhdTSv0Uc3UL1WcuXSklNGBxKRcuUIKuSwuUIYVcuSpUaFCMIAiCJUNqtOz2P+BXY/S7T0IhVYRINWqInTs9j2ZjS57b2ueRtb6K1x+PJPw2cd3/AJRc+At4rz3sJtQ7/wAJ14BLTy1C1nxtwb2pm65s4OEmh+MlJJkbtDWEtotyDXE/zR9+S8y2phiy3WOhsvQMRTLjOufjr981D2nsL/UMlvzjLrwPJCMqZo1wZfsf2ifga4dJNJ1qrOI0cP4hPjcdPfMLXZVY2pTcHNcJDhkQV82bSoOpvDXAggwZ5L0b9lO2ntdVwzjLBD2D8pJO8ByJvHGeK1fuSD5o9OfhGuzVfi8G1uSfrbSaNVDxO0m7syg0bxsr6tABVu1MYGM3G5lFi9oE2YJ++Krm4BzzvPvy0VGXk/YgVCX2Hyj1VB2rpxSK2r8OGhef9tsXvO3BkM+qMOWZZOImOZnPNbf9n+PNKsAT3XaeN/aVi6bM1f7BdcHgR4f2sExk6FIdnqHaPCb1JzmDvNdvQOd3eBBJ6nkqrZG394AONxYO0PIq32biPiUZP5S13PcuD4tJWKptAe9oyDnD+lx/RYJXwWfBq8dUYRJ7vMXHkqSsyDYgjl/ddQqncN8rdR9wmibkJjStqVC+oittnLlySV0hAVchJStUDQcLkq5QhhAuSApUqMihGCgSgogY4EYKbBRAqALzsrV3cRTPOPMEfVemVsPvNteD9+i8w7LtnEU+TgfVexbMoTvHQki+jmmCldRG2MYZUZfFVPhEF2RWg2LQ3h8RlwfmbxHEKP2m2XNFw4H0Nv0VO/aj8M6n8I2EAjQ+CU6dMa7Voi/tS2G34YxNOxaRvRqDAE8wbKk7EVi3Fj+JnLjK3HajFjEbPrP3d0ltx/FIy62WB7NiMWzkB6yrbrVFoqmey0WNeO8AeolQ8VgGaNA6AKTgHWCkVmyEe0aXTM8/Cck5/p4GSsvg3TWIbCFF7MxtqpuU3HkvJtrkvcTzInidSvV+11M/BP3xXmeNoRHIe5/VGDpmeXlFGykZV/sWiPm8D5/2UWjhZbPX/Cn7KpFz2sFySBA1JWk5WjCK5N/2fpfuHuORJPlTE+pCxzoc572G5c4wMiN6behWu21jW0qQwjDeAHkal13R5+3BZZlIMMN/C5oBPBwke0eKziwtcjmGfLfvMfY8k5KjlwaagH5vYx+ia+OntLFJbhLUt3RMJSSonx0nxk3uFtpLlE0qF8ZO0qilgomrk3vJUbIYZKEKVLDDCXJEoCIAmp2mwnIKXg9nzd3krSjQjRPYdDKSufApk1SXEeQOz1MsqtPG08CRb1hes7KqOisT8pFOo3kTYj0XmbGxfhqvRMBX3sJvRcwPAEE+qGr0sIJNfgmnzylJp/ks9sVg/DVDxpz4gwsXg8C7EVAGiwNz0Whr0nOpCllvfNfJkyVMrVqOAw5quALiCGDIuOfgOJ4LgyinN10dmEmor3KLtm9tKgMM03O6Xa90GfUgeRWT7Js38ST9xkFA2ptd9XeqPdLnmeFuQ0GgC1H7OdlbzDVOZsD0Kp0MQVnoWCp91SSOS7D90QVI3mrRUR9jApWUF9OSrKpUEKCXHQKNoMUyq7QYDfpObrEheSYylFQsOth4zB8wQvT+2W2xhaBfnUMtpj+KDpygnwXlFKs6o06uA32GfmGb2k8ZVUubKzdKgsQ3cDbQDc+Nj6n0T2wcUKINaJqGdwflGW91OQ8Sixg+LTluoD28223m9QfQhRaNNwLBFi+lHNu9u+ji3zVqvgyfuPjEO+N3zLpM5zJuT98k+cVvuPNzR/TP9h4qPtul8PEVI5HzAKg0K26LZmw1ic45qKLfRG6JWMxt3EX3nnyGZ9QgpVp5JqnhS7O3AcB+qL4ULt4dG440n2crLqIym6ZICIIaDxkfNTBSWU4OLpkjNSI4CkURdEKScpU7qqC2PQuTu4uVihgUqRcsBkUK42PgZ75HRVNFm84DiVsqFHdZHAJ/Q4lKW9+BLVZKW1CMpSnBRXYZ0yOCl0aJe4NbcldRuuznu/BI2Vsv4pABnV2gaOfHotlULaDO9AAaAxmsDU9UGz6TMNTtBfry5nh7rNbc200THff5gLzuu13xJbYdHZ0WjcI7p9lm3azWh1asYYO8QbbxyY3pqV5/2g7Q1cZVc95IZk1vLkNAk2ti31Q1ru9F40lQWYUuN/H9Fz+kdFLkaALyOZAAXtvZDBfCoMHKfNeYbC2d8SuxsZX6Bez4KnutA5Kq5YzBUrHyETrxYCMvHOUYC4q9E4YDwgFNOylRBZ4d+0TGPr4yoy5bSIYxvINBf4neP9IVXsxooubvnuxvSLmmSSAY1ygjUFbHtlsXdxjnRap3hpPKdHAzHkqPbWyXBm+zh3h+Zsgk9eI58wTTd9LM5LmxmthXUxVpjgX0iLiXfhadRNv6VoNhUadfDsqixbuugX3XWkcwcv8AFqSkC/DFgPea2aZ13ZndnlDfsKX2MxO5UH5Kpc3d074L2/8AYvaI/MeCv8O4t+UZudNLwzP9qqpbWIzkAjmMh6gg8wUOysN+I5njorzt5s2C135XEdQ8SD5t83lV2z290LrfpeOL9fk5uvm0tpI+DFjpzGnAoK1ATlGscAbjPkQpjqXK30Q1m+PHrr6rsnKRVYijB042vC6jXLLHLgfopWIaNB1vr9ByUPENGn+CspwTXJtFss6bwRITtNU2HrljoMxqFcUnLm5Mex/YZi77JSVIlVCx55K6UMrpSwyWOxaW9VHK61ellQdm6fzOV/UMDJdrSRrEvucvUSvIxMNy4q3wWIFOSDDjrwHAczxVThAB4lP1iGscZzKOraWJ30DAm8qSJGP2sd0weWeXPqomFwLS3eqOzyA15DiqxkuJ875KwwWLD3bo0+Z54DgF5Vxtuj0idIJ+DLrtG4wZu/ERwHDwTBoBgJiABYcBxVnVxO+QB8ouB9Tz9lT7Qeaj9xuWvRbYNPLJJRMsueMIuRadktotpPNR7CWnIjMDoV6LhO0mFcP90N5PBb72Xm7GQBa2U9In3C6y7kv03A+rRyofqeePdM9YZtOicq1M9Ht/VBV2vQb81amP+bf1XlRhIAPsLL9qh/0zb92nXyo9GxXazCsycXngxp9zAWe2l2zqvltJopD83zP9oHr1WaKG+X2I+wmcegww5q/yLZdfmn5r8f6x04uXTUcXEmSXHeM8ZOfDzVw2o1zc5B/FnBi08euoMZ3NDUYIkiYm0axGSGlLMgACIIAABHAx76LLW6BZ+Y8Mml1rw8S5QzXpfBqtA+QkwOF4c3p9Emz7VQNG1WuHQPgejx5KTtE7zSHWcBvA82x7j2Vfg8XBqTmN0C3AQfG0rmaXE5On30dHU5Ela6NbtfdxGHbNpAvwu1w9R6lZfZ9GCQcxbLKFdYmpu4ZgOZmw0mY/9N81CwlPU3OvgunocDxOXsc7V51kUfceez2B8It9FHqmTYRllyGd9dUeNqhrd068/CLck2y0HW3TpGUcl0BJe5GxLt69sgLAAW5D7KhYhoc6GiJgQTNzzPNWT6gL9543gTLh8szncZKvqUi7eIiwk3i0jjnmqtGsWV9SZv6q1wFSWjyVU+L+imbMfpz+/ZJ516RqBdLk3vLkmXMDK6UEpWXICXGWarYLd2nMK2qm0KHg27rBAyClvyHRehhHbFI47dyscwjYABzhdtKkXU4HEIsO3VOVZ3SAYkQqZ8fxIOPuHHk+HkUvYzVXEGQWaWKmUWu+VggHMlT6OEa3RIaAkQLcrQlf26FJWMPX5LfAlWrut3Kdz+J+nhxRYTChnM6nmdfvijYwAxqZKcE39PJM4sEMS9IvkzTyP1BCDF7Wvy/wuDoNuY80JdeI0z04QgLg25JuQNTc2EDRbGY4Br95wiPeyAEN0tYRc8TJQt58/ayQ/wCPHJAh1szkAenX3XRIta2eo8EpXCbz4dOahBY8susc9V1S4ENyAkibkk3PM5Ry6pd4Qb3EQImZJm+kfVIKmgOd+QgG44mJA6olWxzadAF4HMT0iT4RPkVSYPDmZIkXJPM5AcevJWeNqwS+bFt50EQR1zCDctAMZSY0m48cvFLYcCxvd54Gcmd5Ft8cjr3l0A/dp8rKQxkAu5eFuSZaeAkn9fZBtbFCnTdxDZ8LwPdMcIXptpFbQrmpWJmzO71OTirP4kSLXtcTGRtwPNV+xaW5SBJue8eZNyVMe6dBb15lVj1yazq6Q1U3YOe9NuEReef91CxVMgAkWIkc7kfRS6okkNl2cWuQOQUGoRInLXjGsKwEQa0WiZ16zojwL+95fVBVN7ev3mhwzu8ks/yscgXu+uUffXJE1oxacw/zN6j3SLljHtG8/lZuGZeA9k67ILly9GzjIkYfJOLlyBV9guQlcuUIzkFXTqPYrlyhAyuYuXIkYh1UjG/Of5af/hq5cqfUv5/osvl/33I1P5n9GexUqhk/+T/7sXLlPH8/2VfZGdmkbmfFKuVyAuyb/N9WIWZ/fELlyqBD2EyHQ+yr+03+07/j/wCmrlypL5H+DTH/AOiHnZeA9wnanyrlyugMTA/P/wAKn/hyqK6Rcp5ZZEZ+Xl9EzT+dIuSWf5WNQLRcuXJE3P/Z" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <img src="https://images.pexels.com/photos/532168/pexels-photo-532168.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="Image" className="img-fluid rounded recent-post-img"/>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-success mb-3">Nature</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left recent-post-figure-img"><img src="https://i.guim.co.uk/img/media/82ca7e3fe3961e2d77d8ed97bf3d28b7bac2ca9b/937_272_5715_3429/master/5715.jpg?width=300&quality=85&auto=format&fit=max&s=a881d059ee37cd2d0d3506ea77a4d2c1" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="https://images.pexels.com/photos/132472/pexels-photo-132472.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="Image" className="img-fluid rounded recent-post-img"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-warning mb-3">Travel</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left recent-post-figure-img"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw7TnJ1VITK6Lvj7DfZ0li6Z9_hHGVji8JSRGYdjT_t2JHCIhn&s" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="images/img_1.jpg" alt="Image" className="img-fluid rounded"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-secondary mb-3">Politics</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="images/img_2.jpg" alt="Image" className="img-fluid rounded"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-success mb-3">Nature</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="images/img_4.jpg" alt="Image" className="img-fluid rounded"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-danger mb-3">Sports</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="images/img_1.jpg" alt="Image" className="img-fluid rounded"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-success mb-3">Nature</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="images/img_2.jpg" alt="Image" className="img-fluid rounded"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-danger mb-3">Sports</span>
                                    <span className="post-category text-white bg-secondary mb-3">Tech</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-4">
                            <div className="entry2">
                                <a href="single.html"><img src="images/img_4.jpg" alt="Image" className="img-fluid rounded"/></a>
                                <div className="excerpt">
                                    <span className="post-category text-white bg-danger mb-3">Sports</span>
                                    <span className="post-category text-white bg-warning mb-3">Lifestyle</span>
                                    <h2><a href="single.html">The AI magically removes moving objects from videos.</a></h2>
                                    <div className="post-meta align-items-center text-left clearfix">
                                        <figure className="author-figure mb-0 mr-3 float-left"><img src="images/person_1.jpg" alt="Image" className="img-fluid"/></figure>
                                        <span className="d-inline-block mt-1">By <a href="#">Carrol Atkinson</a></span>
                                        <span>&nbsp;-&nbsp; July 19, 2019</span>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo sunt tempora dolor laudantium sed optio, explicabo ad deleniti impedit facilis fugit recusandae! Illo, aliquid, dicta beatae quia porro id est.</p>
                                    <p><a href="#">Read More</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row text-center pt-5 border-top">
                        <div className="col-md-12">
                            <div className="custom-pagination">
                                <span>1</span>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                <a href="#">4</a>
                                <span>...</span>
                                <a href="#">15</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>*/
        )
    }
}

export default RecentPosts;