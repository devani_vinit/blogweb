import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "../css/style.css"

class PopularPosts extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="site-section bg-light">
                    <div className="container">
                        <div className="row align-items-stretch retro-layout-2">
                            <div className="col-md-4">
                                <div href="single.html" className="h-entry mb-30 v-height gradient"
                                   style={{backgroundImage: "url(https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)"}}>
                                    <div className="text">
                                        <h2>The AI magically removes moving objects from videos.</h2>
                                        <span className="date">July 19, 2019</span>
                                    </div>
                                </div>
                                <div href="single.html" className="h-entry v-height gradient"
                                   style={{backgroundImage: `url()`}}>
                                    <div className="text">
                                        <h2>The AI magically removes moving objects from videos.</h2>
                                        <span className="date">July 19, 2019</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div href="single.html" className="h-entry img-5 h-100 gradient"
                                   style={{backgroundImage: `url(https://images.pexels.com/photos/2785523/pexels-photo-2785523.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)`}}>
                                    <div className="text">
                                        <div className="post-categories mb-3">
                                            <span className="post-category bg-danger">Travel</span>
                                            <span className="post-category bg-primary">Food</span>
                                        </div>
                                        <h2>The AI magically removes moving objects from videos.</h2>
                                        <span className="date">July 19, 2019</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div href="single.html" className="h-entry mb-30 v-height gradient"
                                   style={{backgroundImage: `url(https://images.pexels.com/photos/3115006/pexels-photo-3115006.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)`}}>
                                    <div className="text">
                                        <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                        <span className="date">July 19, 2019</span>
                                    </div>
                                </div>
                                <div href="single.html" className="h-entry v-height gradient"
                                   style={{backgroundImage: `url('../images/img_4.jpg')`}}>
                                    <div className="text">
                                        <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                        <span className="date">July 19, 2019</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PopularPosts;