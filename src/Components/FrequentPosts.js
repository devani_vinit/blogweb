import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "../css/style.css"

class FrequentPosts extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <div className="site-section bg-light">
                    <div className="container">
                        <div className="row align-items-stretch retro-layout">
                            <div className="col-md-5 order-md-2">
                                <div href="single.html" className="hentry img-1 h-100 gradient"
                                     style={{backgroundImage: `url("https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500")`}}>
                                    <span className="post-category text-white bg-danger">Travel</span>
                                    <div className="text">
                                        <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                        <span>February 12, 2019</span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-7">
                                <div href="single.html" className="hentry img-2 v-height mb30 gradient"
                                     style={{backgroundImage: `url("https://images.pexels.com/photos/853199/pexels-photo-853199.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500")`}}>
                                    <span className="post-category text-white bg-success">Nature</span>
                                    <div className="text text-sm">
                                        <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                        <span>February 12, 2019</span>
                                    </div>
                                </div>
                                <div className="two-col d-block d-md-flex">
                                    <a href="single.html" className="hentry v-height img-2 gradient"
                                         style={{backgroundImage: `url("https://images.pexels.com/photos/757889/pexels-photo-757889.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500")`}}>
                                        <span className="post-category text-white bg-primary">Sports</span>
                                        <div className="text text-sm">
                                            <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                            <span>February 12, 2019</span>
                                        </div>
                                    </a>
                                    <a href="single.html" className="hentry v-height img-2 ml-auto gradient"
                                         style={{backgroundImage: `url('../images/img_3.jpg')`}}>
                                        <span className="post-category text-white bg-warning">Lifestyle</span>
                                        <div className="text text-sm">
                                            <h2>The 20 Biggest Fintech Companies In America 2019</h2>
                                            <span>February 12, 2019</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FrequentPosts;